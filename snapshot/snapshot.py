"""
Make snapshot

{"Tasks": {"total": 440, "running": 1, "sleeping": 354, "stopped": 1, "zombie": 0},
"%CPU": {"user": 14.4, "system": 2.2, "idle": 82.7},
"KiB Mem": {"total": 16280636, "free": 335140, "used": 11621308},
"KiB Swap": {"total": 16280636, "free": 335140, "used": 11621308},
"Timestamp": 1624400255}
"""


import argparse
import psutil
import time
import calendar
import json
import os


class Snapshot():
    def __init__(self, data):
        """Initiate class Snapshot"""
        self.snapshotdata = data

    def display(self):
        print(self.snapshotdata)


def main():
    snapshot = {}
    i = 0
    """Snapshot tool."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Interval between snapshots in seconds", type=int, default=5)
    parser.add_argument("-f", help="Output file name", default="snapshot.json")
    parser.add_argument("-n", help="Quantity of snapshot to output", default=20)
    args = parser.parse_args()
    file = open(args.f, "w")
    while i < int(args.n):
        valuetime = time.gmtime()
        ts = calendar.timegm(valuetime)
        virtual_memory = psutil.virtual_memory()
        swap_memory = psutil.swap_memory()
        cpu_info = psutil.cpu_times_percent()
        count_running = 0
        count_sleeping = 0
        count_zombie = 0
        count_stopped = 0
        count_total = 0
        for p in psutil.process_iter(['name', 'status']):
            count_total = count_total + 1
            if p.info['status'] == psutil.STATUS_RUNNING:
                count_running = count_running + 1
            elif p.info['status'] == psutil.STATUS_SLEEPING:
                count_sleeping = count_sleeping + 1
            elif p.info['status'] == psutil.STATUS_ZOMBIE:
                count_zombie = count_zombie + 1
            elif p.info['status'] == psutil.STATUS_STOPPED:
                count_stopped = count_stopped + 1
        snapshot["Tasks"] = {
            'total': count_total,
            'running': count_running,
            'sleeping': count_sleeping,
            'stopped': count_stopped,
            'zombie': count_zombie}
        snapshot['%CPU'] = {
            'user': cpu_info.user,
            'system': cpu_info.system,
            'idle': cpu_info.idle}
        snapshot['KiB Mem'] = {
            'total': virtual_memory.total // 1024,
            'free': virtual_memory.free // 1024,
            'used': virtual_memory.used // 1024}
        snapshot['KiB Swap'] = {
            'total': swap_memory.total // 1024,
            'free': swap_memory.free // 1024,
            'used': swap_memory.used // 1024}
        snapshot['Timestamp'] = ts
        time.sleep(args.i)
        os.system('clear')
        SnapshotClass = Snapshot(snapshot)
        SnapshotClass.display()
        print(snapshot, end="\r")
        i += 1
        json.dump(snapshot, file)
        file.write('\n')


if __name__ == "__main__":
    main()
