from setuptools import setup, find_packages

setup(
    name='snapshot',
    install_requires=[
        'psutil==5.9.2',
    ],
    packages=find_packages(),
    version="0.1",
    author="Dzmitry_Bichko",
    author_email="dzmitry_bichko@epam.com",
    description="Python Lab from EPAM")
