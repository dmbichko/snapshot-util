For monitoring purposes use **psutil** module, see: https://pypi.org/project/psutil/ 

This module should snapshots of the state of the system each 30 seconds (configurable):

    {"Tasks": {"total": 440, "running": 1, "sleeping": 354, "stopped": 1, "zombie": 0},
    "%CPU": {"user": 14.4, "system": 2.2, "idle": 82.7},
    "KiB Mem": {"total": 16280636, "free": 335140, "used": 11621308},
    "KiB Swap": {"total": 16280636, "free": 335140, "used": 11621308},
    "Timestamp": 1624400255}

Output written to the console and  json file.

The script has to accept an interval (default value = 30 seconds) and output file name. 

-i, help="Interval between snapshots in seconds", type=int, default=30)
-f, help="Output file name", default="snapshot.json")
-n , help="Quantity of snapshot to output", default=20)

